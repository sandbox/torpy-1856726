<?php

/**
 * Build and return an iCal feed.
 */
function demonz_calendar_get_ical_feed($calendar_id, $date) {
  global $language;
  // DB caching.
  $cache_key = $calendar_id . ':' . $date . ':' . $language->language;
  $feed = &drupal_static(__FUNCTION__ . ':' . $cache_key);

  if (!isset($feed)) {
    if ($cache = cache_get('demonz_calendar_ical:' . $cache_key)) {
      $feed = $cache->data;
    }
    else {
      $feed_data = demonz_calendar_get_ical_data($calendar_id, $date);
      $feed = theme('demonz_calendar_ical', array(
        'events' => $feed_data,
        'calendar_id' => $calendar_id,
        'date' => $date,
      ));

      // Cache lifetime.
      $extra_cache_time = (int) variable_get('demonz_calendar_cache_lifetime', (3600 * 24 * 7));

      // Don't cache if set as such.
      if ($extra_cache_time != 0) {
        $cache_expiry = REQUEST_TIME + $extra_cache_time;
        cache_set('demonz_calendar_ical:' . $cache_key, $co);
      }
    }
  }

  print $feed;
}

function demonz_calendar_get_ical_data($calendar_id, $date) {
  global $language;
  $events = array();
  $settings = _demonz_calendar_get_settings($calendar_id);

  // Lets get the view, first get the proper view/display name.
  $settings['view'] = explode(':', $settings['view']);
  $settings['view_display'] = !empty($settings['view'][1]) ? $settings['view'][1] : 'default';
  $settings['view'] = $settings['view'][0];

  // Then, load the view.
  $view = views_get_view($settings['view']);

  // Get timezone data.
  list($timezone_location, $timezone_offset, $timezone) = _demonz_calendar_get_timezone();

  // Get date parts.
  list($year, $month, $day) = _demonz_calendar_get_date_parts($date);

  // Re-assemble!
  $date = array(
    $year,
    $month,
    $day
  );

  // Get the base query, we'll be extending this to get more event data.
  $query = _demonz_calendar_get_base_calendar_query($settings, $view, $year, $month);

  // Remove unneeded expressions (speed up query).
  $query_expressions = &$query->getExpressions();
  if (isset($query_expressions['date_day'])) {
    unset($query_expressions['date_day']);
  }
  if (isset($query_expressions['date_month'])) {
    unset($query_expressions['date_month']);
  }
  if (isset($query_expressions['date_year'])) {
    unset($query_expressions['date_year']);
  }

  // Get the event end date.
  foreach ($settings['date_fields'] as $date_field) {
    $date_field_alias = $date_field . '_table';
    $query->addField($date_field_alias, $date_field . '_value2');
  }

  // We need the title as well (has support for title module!).
  // if (module_exists('title')) {
  //   $title_field_table = 'field_data_title_field';
  //   $query->join($title_field_table, $title_field_table, 'n.nid = ' . $title_field_table . '.entity_id AND ' . $title_field_table . '.entity_type = :node AND ' . $title_field_table . '.deleted = :deleted', array(
  //     ':node' => 'node',
  //     ':deleted' => '0',
  //   ));
  //   $query->fields($title_field_table, array('title_field_value'));
  // }
  // @TODO: add support for title module.
  $query->addField('n', 'title');

  // @TODO: location field support.

  $events = $query->execute()->fetchAll();

  $ical_date_format = 'Ymd\THis';

  // Post-process events.
  foreach ($events as $key => &$event) {
    foreach ($settings['date_fields'] as $date_field) {
      $start_date = $event->{$date_field . '_value'} . ' UTC';
      $start_time = strtotime($start_date);
      $event->start_time = date($ical_date_format, $start_time);

      $end_date = $event->{$date_field . '_value2'} . ' UTC';
      $end_time = strtotime($end_date);

      if ($end_time == $start_time) {
        // If the start time is the same as the end time, this is an all-day
        // event.
        // Add on a day, as per the iCal spec (RFC 2445, clarified in RFC 5545).
        $end_time += (3600 * 24);
      }

      $event->end_time = date($ical_date_format, $end_time);
    }

    $event->url = url('node/' . $event->nid, array(
      'absolute' => TRUE,
    ));
  }

  return $events;
}
