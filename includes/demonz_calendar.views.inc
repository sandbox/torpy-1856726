<?php

/**
 * Implements hook_views_pre_view().
 */
function demonz_calendar_views_pre_view(&$view, &$display_id, &$args) {
  // Performance optimisation: prevent this function from running
  // multiple times for ignored views.
  $ignored_views = &drupal_static(__FUNCTION__ . ':ignored', array());
  if (isset($ignored_views[$view->name . ':' . $display_id])) { return; }

  // Get date argument if its set.
  $date = (isset($_REQUEST['calendardate'])) ? filter_input(INPUT_GET, 'calendardate') : 'now';

  $settings = _demonz_calendar_get_settings();

  // Attach calendar to views (where required).
  if (!empty($settings['calendars'])) {
    foreach ($settings['calendars'] as $calendar) {
      $view_name = explode(':', $calendar['view']);
      $view_display = !empty($view_name[1]) ? $view_name[1] : 'default';
      $view_name = $view_name[0];

      // Non-matching view/display.
      if ($view->name != $view_name || $display_id != $view_display) {
        $ignored_views[$view->name . ':' . $display_id] = TRUE;
        continue;
      }

      if ($calendar['view_attach'] === 1) {
        $co = demonz_calendar_get_calendar($calendar['id'], $date);
        $view->attachment_before .= $co;
        break;
      }
    }
  }

  if ($date != 'now') {
    // If a calendar date filter is set, remove all other date filters.
    // (we don't want them conflicting).
    $filters = $view->get_items('filter', $display_id);
    foreach ($filters as $id => $filter) {
      // Sometimes date filters are set using 'node' as a base table.
      if (isset($filter['date_fields'])) {
        $view->set_item($display_id, 'filter', $id, NULL);
      }
      else {
        // Assume we're using the SQL storage and ignore and any fields that don't
        // have the 'field_data_' table prefix.
        // Unfortunately, this is the only way to get the field name since we only
        // have access to the table and column names at this point.
        // May not work with non-SQL backends.
        if (strpos($filter['table'], 'field_data') !== 0) { continue; }
        $field_name = str_replace('field_data_', '', $filter['table']);

        // Check against the field name to see if this filter's for a date field.
        // Remove if so.
        $field_info = field_info_field($field_name);
        if ($field_info['module'] == 'date') {
          $view->set_item($display_id, 'filter', $id, NULL);
        }
      }
    }
  }
}

/**
 * Implements hook_view_query_alter().
 */
function demonz_calendar_views_query_alter(&$view, &$query) {
  $display_id = $view->display_handler->display->id;

  // Performance optimisation: prevent this function from running
  // multiple times for ignored views.
  $ignored_views = &drupal_static(__FUNCTION__ . ':ignored', array());
  if (isset($ignored_views[$view->name . ':' . $display_id])) { return; }

  $settings = _demonz_calendar_get_settings();
  $view_enabled = FALSE;

  // Check if this view is enabled.
  foreach ($settings['calendars'] as $calendar_setting) {
    $view_name = explode(':', $calendar_setting['view']);
    $calendar_setting['view_name'] = $view_name[0];
    $calendar_setting['view_display'] = !empty($view_name[1]) ? $view_name[1] : 'default';
    $view_display = $calendar_setting['view_display'];
    $view_name = $view_name[0];

    if ($view->name == $view_name && $view_display == $display_id) {
      $view_enabled = TRUE;
      break;
    }
  }

  // Store this ignored view so we can skip faster on subsequent runs.
  if (!$view_enabled) {
    $ignored_views[$view->name . ':' . $display_id] = TRUE;
    return;
  }

  $date = (isset($_REQUEST['calendardate'])) ? filter_input(INPUT_GET, 'calendardate') : 'now';

  if ($date == 'now') {
    return;
  }

  // Add our signatures on.
  $query->add_tag('demonz_calendar_view');
  $query->add_tag('demonz_calendar_view_' . $calendar_setting['id']);

  // Get timezone data.
  list($timezone_location, $timezone_offset, $timezone) = _demonz_calendar_get_timezone();

  // Get date parts.
  list($year, $month, $day) = _demonz_calendar_get_date_parts($date, FALSE);

  // Re-assemble!
  $date = array(
    $year,
    $month,
    $day
  );

  // Run per date field (we can have multiple ones).
  foreach ($calendar_setting['date_fields'] as $date_field) {
    $date_field_table = 'field_data_' . $date_field;
    $date_field_relationship = $date_field . '_relationship';

    // Again with the massive assumptions, tack on a join to this table.
    $date_field_join = new views_join();
    $date_field_join->construct($date_field_table, 'node', 'nid', 'entity_id', array(), 'INNER');
    $query->add_relationship($date_field_table, $date_field_join, 'node');

    // Add on the filter query parts.
    $query->add_where_expression('demonz_calendar', _demonz_calendar_get_sql_timezone_math('year', $timezone_offset, $date_field_table, $date_field) . ' = :year', array(
      ':year' => $year,
    ));
    if ($month != -1) {
      $query->add_where_expression('demonz_calendar', _demonz_calendar_get_sql_timezone_math('month', $timezone_offset, $date_field_table, $date_field) . ' = :month', array(
        ':month' => $month,
      ));

      if ($day != -1) {
        $query->add_where_expression('demonz_calendar', _demonz_calendar_get_sql_timezone_math('day', $timezone_offset, $date_field_table, $date_field) . ' = :day', array(
          ':day' => $day,
        ));
      }
    }
  }
}
