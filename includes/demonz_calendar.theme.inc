<?php

function theme_demonz_calendar_calendar($variables) {
  $calendar = $variables['calendar'];
  $calendar_id = $variables['calendar_id'];

  $settings = $calendar['#settings'];
  unset($calendar['#settings']);

  $calendar_url = $settings['view_url'];

  $ical = $calendar['#ical'];
  unset($calendar['#ical']);

  $year = $calendar[1]['year'];
  $month = $calendar[1]['month_numeric'];

  // Start building output with JS data.
  $out = '<div class="demonz-calendar" data-cid="' . $calendar_id . '" data-dateyear="' . $year . '" data-datemonth="' . $month . '">';

  // Get prev/next month (and year!).
  $prev_date = (int) $month;
  $prev_date--;
  $prev_year = $year;
  if ($prev_date < 1) {
    $prev_date = 12 + $prev_date;
    $prev_year = (int) $year - 1;
  }
  $prev_date = ($prev_date < 10) ? '0' . $prev_date : $prev_date;
  $prev_date = "{$prev_year}-{$prev_date}";

  $next_date = (int) $month;
  $next_date++;
  $next_year = $year;
  if ($next_date > 12) {
    $next_date = $next_date - 12;
    $next_year = (int) $year + 1;
  }
  $next_date = ($next_date < 10) ? '0' . $next_date : $next_date;
  $next_date = "{$next_year}-{$next_date}";

  // Heading.
  $header = '<div class="calendar-heading">';
  $header .= theme('demonz_calendar_header_operations', array(
    'calendar_id' => $calendar_id,
    'current_month' => $month,
    'current_year' => $year,
    'month_range' => 'all',
    'year_range' => '+3:-3',
  ));
  // $header .= '<h3>' . $calendar[1]['month'] . '</h3>';
  $header .= theme('demonz_calendar_header_month_pager', array(
    'calendar_id' => $calendar_id,
    'calendar_url' => $calendar_url,
    'prev_date' => $prev_date,
    'next_date' => $next_date,
  ));
  $header .= '</div>';
  $out .= $header;

  // Calendar.
  $days = _demonz_calendar_get_ordered_days($calendar_id);
  $content = '<div class="calendar-content"><table>';

  // Add accessibility caption to calendar.
  $calendar_caption = '<caption class="element-invisible">' . t('Calendar for @date', array(
    '@date' => $calendar[1]['month'],
  )) . '</caption>';
  $content .= $calendar_caption;

  // Get header days.
  $content .= theme('demonz_calendar_header_days', array(
    'calendar_id' => $calendar_id,
  ));

  $content .= '<tbody>';
  $content .= '<tr>';

  $col_count = 0;
  foreach ($calendar as $k => $c) {
    if ($col_count == 7) {
      $content .= '<tr>';
      $col_count = 0;
    }

    if ($k == 1) {
      $start_pos = array_search($c['day'], $days);
      if ($start_pos > 0) {
        for ($i=0; $i < $start_pos; $i++) {
          $content .= '<td class="' . strtolower($days[$i]) . ' empty"></td>';
          $col_count++;
        }
      }
    }

    $classes = array(
      strtolower($days[$col_count]),
      ($c['events']) ? 'has-events' : 'no-events',
      ($c['today']) ? 'today' : '',
      ($c['current']) ? 'active': '',
    );
    $classes = implode(' ', $classes);

    $content .= '<td class="' . $classes . '">';
    if ($c['events'] || $c['today']) {
      $date_q = ($c['today']) ? 'now' : $c['date_link'];
      $content .= l($k, $calendar_url, array(
        'query' => array(
          'calendardate' => $date_q,
        )
      ));
    }
    else {
      $content .= '<div class="inner">' . $k . '</div>';
    }
    $content .= '</td>';

    $col_count++;

    if ($col_count == 7) {
      $content .= '</tr>';
    }
  }

  // If the column count isn't '7' yet, we need to fill the rest of the
  // calendar table with empty <td>s and close tr.
  if ($col_count != 7) {
    for ($i = $col_count; $i < 7; $i++) {
      $content .= '<td></td>';
    }
    $content .= '</tr>';
  }

  $content .= '</tbody>';
  $content .= '</table></div>';
  $out .= $content;

  // Tack on iCal support if needed.
  if ($ical === TRUE) {
    $out .= theme('demonz_calendar_ical_link', array(
      'date' => $year . '-' . $month,
      'calendar_id' => $calendar_id,
    ));
  }

  $out .= '</div>';

  return $out;
}

/**
 * Return a list of days for the calendar header.
 */
function theme_demonz_calendar_header_days($variables) {
  $out = '';

  // Form: class => name.
  $days = array(
    'mon' => 'M',
    'tue' => 'T',
    'wed' => 'W',
    'thu' => 'T',
    'fri' => 'F',
    'sat' => 'S',
    'sun' => 'S',
  );

  $out .= '<thead><tr>';

  foreach ($days as $class => $name) {
    $out .= '<th class="days ' . $class . '">' . t($name) . '</th>';
  }

  $out .= '</tr></thead>';

  return $out;
}

/**
 * Return the month/year dropdown forms.
 */
function theme_demonz_calendar_header_operations($variables) {
  $current_month = (int) $variables['current_month'];
  $current_year = (int) $variables['current_year'];
  $month_range = $variables['month_range'];
  $year_range = $variables['year_range'];

  // Get month range.
  if ($month_range == 'all') {
    $min_month = 1;
    $max_month = 12;
  }
  else {
    $month_range = explode(':', $month_range);
    $max_month = $current_month + (int) $month_range[0];
    $max_month = ($max_month > 12) ? 12 : $max_month;
    $min_month = $current_month + (int) $month_range[1];
    $min_month = ($min_month < 1) ? 1 : $min_month;
  }

  // Get year range.
  if (empty($year_range)) {
    // Safe defaults
    $year_range = '+3:-3';
  }

  $year_range = explode(':', $year_range);
  $max_year = $current_year + (int) $year_range[0];
  $min_year = $current_year + (int) $year_range[1];

  $months = array();
  $years = array();

  // From http://stackoverflow.com/a/7229192/356237.
  // Build friendly months.
  for ($i = $min_month; $i <= $max_month; $i++) {
    $ci = ($i < 10) ? '0' . $i : $i;
    $months[$ci] = strftime('%B', mktime(0, 0, 0, $i, 1));
  }

  // Build years.
  for ($i = $min_year; $i <= $max_year; $i++) {
    $years[$i] = $i;
  }

  $date_form = drupal_get_form('demonz_calendar_date_dropdown_form', array(
    'current_year' => $current_year,
    'current_month' => $current_month,
    'months' => $months,
    'years' => $years,
  ));

  return drupal_render($date_form);
}

/**
 * Return the themed prev/next/upcoming links
 */
function theme_demonz_calendar_header_month_pager($variables) {
  $out = '';

  $calendar_url = $variables['calendar_url'];
  $prev_date = $variables['prev_date'];
  $next_date = $variables['next_date'];

  $out .= '<ul class="month-pager">';

  $out .= '<li class="calendar-prev">' . l('<i class="icon-previous" aria-hidden="true"></i><span class="label">' . t('Previous month') . '</span>', $calendar_url, array(
  'html' => TRUE,
    'query' => array(
      'calendardate' => $prev_date,
    ),
  )) . '</li>';

  $out .= '<li class="calendar-next">' . l('<i class="icon-next" aria-hidden="true"></i><span class="label">' . t('Next month') . '</span>', $calendar_url, array(
  'html' => TRUE,
    'query' => array(
      'calendardate' => $next_date,
    ),
  )) . '</li>';

  $out .= '<li class="calendar-now">' . l('<i class="icon-back-today" aria-hidden="true"></i>' . t('Upcoming Events'), $calendar_url, array(
  'html' => TRUE,
    'query' => array(
      'calendardate' => 'now',
    ),
  )) . '</li>';

  $out .= '</ul>';

  return $out;
}

function theme_demonz_calendar_ical_link($variables) {
  $calendar_id = $variables['calendar_id'];
  $date = $variables['date'];
  $url = 'demonz-calendar/ical/' . $calendar_id . '/' . $date;

  $out = '<div class="ical">';

  drupal_add_html_head_link(array(
    'rel' => 'alternate',
    'type' => 'application/calendar',
    'title' => 'iCal',
    'href' => url($url)
  ));

  $ical_icon = theme('image', array(
    'path' => drupal_get_path('module', 'demonz_calendar') . '/assets/images/ical.png',
    'width' => 16,
    'height' => 16,
    'alt' => t('iCal icon'),
    'title' => t('Download iCal feed'),
  ));
  $out .= l($ical_icon . '<span>' . t('iCal') . '</span>', $url, array(
    'html' => TRUE,

  ));

  $out .= '</div>';

  return $out;
}

/**
 * Cobbled from:
 * http://stackoverflow.com/q/1463480/356237
 * http://stevethomas.com.au/php/how-to-build-an-ical-calendar-with-php-and-mysql.html
 * http://www.kanzaki.com/docs/ical/
 * The date_ical module.
 */
function theme_demonz_calendar_ical($variables) {
  $calendar_id = $variables['calendar_id'];
  $date = $variables['date'];
  $events = $variables['events'];
  $filename = $calendar_id . '_' . $date;
  $title = variable_get('site_name', t('Drupal'));
  $site_url = url('', array('absolute' => TRUE));
  $site_url = str_replace(array('http://', 'https://'), '', $site_url);
  $site_url = rtrim($site_url, '/');
  $out = '';

  drupal_add_http_header('Content-Type', 'text/calendar; charset=utf-8');
  drupal_add_http_header('Content-Disposition', 'inline; filename="' . $filename . '.ics"');

  $out .= "BEGIN:VCALENDAR\r\n";
  $out .= "VERSION:2.0\r\n";
  $out .= "PRODID:-//Demonz Calendar//iCal Feed//NONSGML v1.0//EN\r\n";
  $out .= "METHOD:PUBLISH\r\n";
  $out .= "X-WR-CALNAME;VALUE=TEXT:$title\r\n";

  if (!empty($events)) {
    foreach ($events as $event) {
      $out .= theme('demonz_calendar_ical_event', array(
        'event' => $event,
        'site_url' => $site_url,
      ));
    }
  }

  $out .= "END:VCALENDAR";

  return $out;
}

function theme_demonz_calendar_ical_event($variables) {
  $event = $variables['event'];
  $site_url = $variables['site_url'];
  $out = "BEGIN:VEVENT\r\n";

  $out .= "DTSTAMP:" . gmdate('Ymd').'T'. gmdate('His') . "Z\r\n";
  $out .= "UID:" . md5(uniqid(mt_rand(), true)) . "@$site_url\r\n";
  $out .= "DTSTART:{$event->start_time}\r\n";
  $out .= "DTEND:{$event->end_time}\r\n";
  $out .= "URL;VALUE=URI:{$event->url}\r\n";
  $out .= "SUMMARY:{$event->title}\r\n";

  $out .= "END:VEVENT\r\n";

  return $out;
}

function demonz_calendar_date_dropdown_form($form, &$form_state, $data) {
  $current_month = (int) $data['current_month'];
  if ($current_month < 10) {
    $current_month = '0' . $current_month;
  }

  $form['demonz_calendar_month'] = array(
    '#title' => t('Calendar Month'),
    '#type' => 'select',
    '#options' => $data['months'],
    '#default_value' => $current_month,
  );

  $form['demonz_calendar_year'] = array(
    '#title' => t('Calendar Year'),
    '#type' => 'select',
    '#options' => $data['years'],
    '#default_value' => $data['current_year'],
  );

  $form['demonz_calendar_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Apply'),
  );

  return $form;
}

function demonz_calendar_date_dropdown_form_submit($form, &$form_state) {
  $month = $form_state['values']['demonz_calendar_month'];
  $year = $form_state['values']['demonz_calendar_year'];

  $date = "{$year}-{$month}";

  $current_page = $_GET['q'];

  drupal_goto($current_page, array(
    'query' => array(
      'calendardate' => $date,
    ),
  ));
}
