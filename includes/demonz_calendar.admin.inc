<?php

/**
 * Admin settings form.
 */
function demonz_calendar_settings_form($form, &$form_state) {
  // Load settings from DB.
  // variable_del('demonz_calendar_calendars');
  // variable_del('demonz_calendar_count');
  $settings = (isset($form_state['#calendar_settings'])) ? $form_state['#calendar_settings'] : _demonz_calendar_get_settings();

  $form['description'] = array(
    '#markup' => '<div class="description">' . t('All calendar settings are stored in the variables table and can be exported via Features (with Strongarm).<br />Timezone is loaded from the site timezone, alternatively, you can define %alter in a custom module (@see).', array(
      '%alter' => 'hook_demonz_calendar_timezone_location_alter()',
      '@see' => '@see _demonz_calendar_get_timezone()',
    )) . '</div><br />',
  );

  // Merge form state into settings.
  if (!empty($form_state['values']['demonz_calendar_calendars']) && is_array($form_state['values']['demonz_calendar_calendars'])) {
    $settings['calendars'] = $form_state['values']['demonz_calendar_calendars'] + $settings['calendars'];
  }

  // For later, this is stupidly helpful.
  $form_state['#calendar_settings'] = $settings;

  // Number of defined calendars.
  $calendar_count = (isset($form_state['#calendar_count'])) ? $form_state['#calendar_count'] : $settings['count'];
  $form_state['#calendar_count'] = (int) $calendar_count;

  $date_fields = _demonz_calendar_get_date_fields();
  $all_views = views_get_views_as_options(FALSE, 'enabled', NULL, TRUE);
  array_unshift($all_views, t('-- Select --'));

  // Cache lifetime, separate variable loading since we don't fancy setting
  // loading here.
  $form['demonz_calendar_cache_lifetime'] = array(
    '#type' => 'select',
    '#title' => t('Calendar cache lifetime'),
    '#description' => t('Applies to all defined calendars.'),
    '#options' => _demonz_calendar_get_cache_lifetime_options(),
    '#default_value' => variable_get('demonz_calendar_cache_lifetime', (3600 * 24 * 7)),
  );

  $form['demonz_calendar_count'] = array(
    '#type' => 'hidden',
    '#default_value' => $calendar_count,
  );

  // '#tree' makes Drupal save all these settings in a serialized variable.
  // We don't need to muck around with custom tables (win!).
  // Added bonus: Features support!
  $form['demonz_calendar_calendars'] = array(
    '#tree' => TRUE,
  );

  for ($i = 0; $i < $calendar_count; $i++) {
    // Unique ID for this calendar, generated via user_password(). Easy!
    $calendar_id = !empty($settings['calendars'][$i]['id']) ? $settings['calendars'][$i]['id'] : strtolower(user_password());

    $form['demonz_calendar_calendars'][$i] = array(
      '#type' => 'fieldset',
      '#title' => t('Calendar %id', array('%id' => $calendar_id)),
      '#description' => t('A block will be provided for this calendar.'),
      '#collapsible' => TRUE,
      '#tree' => TRUE,
    );

    if (empty($settings['calendars'][$i]) || !isset($settings['calendars'][$i]['#saved'])) {
      $form['demonz_calendar_calendars'][$i]['#description'] .= '<br /><span class="warning">' . t('This calendar record is temporary and has not been saved.') . '</span>';
    }

    $form['demonz_calendar_calendars'][$i]['id'] = array(
      '#title' => t('Calendar ID'),
      '#description' => t('A random, unique string that sets this calendar apart.'),
      '#type' => 'textfield',
      '#disabled' => TRUE,
      '#value' => $calendar_id,
      '#attributes' => array(
        'disabled' => 'disabled',
      ),
    );

    $form['demonz_calendar_calendars'][$i]['date_fields'] = array(
      '#title' => t('Date field(s)'),
      '#description' => t('The date field(s) that provide data for this calendar. <strong>Make sure this matches the view (below)!</strong>'),
      '#type' => 'select',
      '#multiple' => TRUE,
      '#options' => $date_fields,
      '#default_value' => !empty($settings['calendars'][$i]['date_fields']) ? $settings['calendars'][$i]['date_fields'] : NULL,
    );

    $form['demonz_calendar_calendars'][$i]['view'] = array(
      '#title' => t('View'),
      '#description' => t('View to link calendar to (required!).'),
      '#type' => 'select',
      '#options' => $all_views,
      '#default_value' => !empty($settings['calendars'][$i]['view']) ? $settings['calendars'][$i]['view'] : 0,
    );

    $form['demonz_calendar_calendars'][$i]['view_url'] = array(
      '#title' => t('View URL'),
      '#description' => t('<strong>Optional:</strong> Override the URL of the view here (e.g. when the view is a block). Supports urls in the same format as <a href="@url">url()</a>.', array(
        '@url' => url('http://api.drupal.org/api/drupal/includes%21common.inc/function/url/7'),
      )),
      '#type' => 'textfield',
      '#default_value' => !empty($settings['calendars'][$i]['view_url']) ? $settings['calendars'][$i]['view_url'] : NULL,
    );

    $form['demonz_calendar_calendars'][$i]['view_attach'] = array(
      '#title' => t('Attach to view directly'),
      '#description' => t('This attaches the calendar directly to the view (via attachment_before). Only use this if you are not already displaying a block on the view page.'),
      '#type' => 'checkbox',
      '#default_value' => !empty($settings['calendars'][$i]['view_attach']) ? $settings['calendars'][$i]['view_attach'] : FALSE,
    );

    $form['demonz_calendar_calendars'][$i]['ical'] = array(
      '#title' => t('Enable iCal feed'),
      '#description' => t('Shows an iCal feed icon under the calendar.'),
      '#type' => 'checkbox',
      '#default_value' => !empty($settings['calendars'][$i]['ical']) ? $settings['calendars'][$i]['ical'] : FALSE,
    );

    // Add delete button if record exists.
    if (!empty($settings['calendars'][$i]) && isset($settings['calendars'][$i]['#saved'])) {
      $form['demonz_calendar_calendars'][$i]['delete'] = array(
        '#id' => 'delete-calendar-' . $i,
        '#cid' => $form['demonz_calendar_calendars'][$i]['id']['#value'],
        '#type' => 'submit',
        '#value' => t('Delete calendar @id' , array('@id' => $calendar_id)),
        '#submit' => array('demonz_calendar_settings_form_delete_calendar'),
      );
    }
  }

  $form['add_more'] = array(
    '#type' => 'submit',
    '#value' => t('Add another calendar'),
    '#submit' => array('demonz_calendar_settings_form_add_calendar'),
  );

  $form = system_settings_form($form);

  $form['#submit'][] = 'demonz_calendar_settings_form_postsubmit';

  return $form;
}

function demonz_calendar_settings_form_validate($form, &$form_state) {
  $calendar_count = 0;
  foreach ($form_state['values']['demonz_calendar_calendars'] as $id => $calendar) {
    $calendar_found = TRUE;

    if (!empty($calendar['date_fields']) && empty($calendar['view'])) {
      form_set_error('demonz_calendar_calendars[' . $id . '][view]', t('Please select a view for calendar with ID %id.', array(
        '%id' => $calendar['id'],
      )));
    }
    elseif (empty($calendar['date_fields'])) {
      unset($form_state['values']['demonz_calendar_calendars'][$id]);
      $calendar_found = FALSE;
    }

    if ($calendar_found) {
      $calendar_count++;
    }
  }

  // Make sure calendar count is up-to-date.
  $form_state['values']['demonz_calendar_count'] = $calendar_count;
}

/**
 * Post-submit to clear block/views/calendar cache.
 */
function demonz_calendar_settings_form_postsubmit($form, &$form_state) {
  cache_clear_all('*', 'cache_block', TRUE);
  cache_clear_all('*', 'cache_views', TRUE);
  cache_clear_all('*', 'cache_views_data', TRUE);
  cache_clear_all('demonz_calendar', 'cache', TRUE);
  drupal_set_message('Block, Views and Calendar cache cleared.');
}

/**
 * Delete calendar record submit callback.
 */
function demonz_calendar_settings_form_delete_calendar($form, &$form_state) {
  $calendar_id = str_replace('delete-calendar-', '', $form_state['triggering_element']['#id']);

  // Get cached settings.
  // At this point we're discarding any unsaved changes, tough luck.
  $settings = $form_state['#calendar_settings'];

  // Unfortunately, we can't preserve calendar IDs.
  unset($settings['calendars'][$calendar_id]);
  $settings['calendars'] = array_values($settings['calendars']);
  $settings['count'] = count($settings['calendars']);

  // Save settings.
  variable_set('demonz_calendar_calendars', $settings['calendars']);
  variable_set('demonz_calendar_count', $settings['count']);

  drupal_set_message(t('Calendar deleted.'));
}

/**
 * Add calendar record submit callback.
 */
function demonz_calendar_settings_form_add_calendar($form, &$form_state) {
  $form_state['#calendar_count']++;
  $form_state['rebuild'] = TRUE;
}

/**
 * Get options for cache lifetime.
 * @param $raw_lifetime
 *        If specified, returns the respective pretty value.
 */
function _demonz_calendar_get_cache_lifetime_options($raw_lifetime = NULL) {
  // Cache lifetime in seconds.
  $lifetime = array(
    0 => t('No caching'),
    3600 => t('1 hour'),
    (3600 * 24) => t('1 day'),
    (3600 * 24 * 7) => t('1 week'),
    (3600 * 24 * 7 * 30) => t('1 month'),
  );

  if (isset($raw_lifetime) && isset($lifetime[$raw_lifetime])) {
    $lifetime = $lifetime[$raw_lifetime];
  }

  return $lifetime;
}
