(function ($) {

  Drupal.behaviors.demonzCalendar = {
    attach: function (context, settings){
      $('.demonz-calendar', context).once('demonz-calendar', function () {
        var $calendar = $(this);
        var calendarID = $calendar.data('cid');
        var year = parseInt($calendar.data('dateyear'));
        var month = parseInt($calendar.data('datemonth'));

        Drupal.demonzCalendar.apiUrl = settings.basePath + 'demonz-calendar/api/';

        // Attach click handlers to prev/next.
        $calendar.find('.calendar-prev a').click(function(e) {
          month--;
          if (month < 1) {
            year--;
            month = 12;
          }
          Drupal.demonzCalendar.replaceCalendar(calendarID, year, month, $calendar);
          e.preventDefault();
        });
        $calendar.find('.calendar-next a').click(function(e) {
          month++;
          if (month > 12) {
            year++;
            month = 1;
          }
          Drupal.demonzCalendar.replaceCalendar(calendarID, year, month, $calendar);
          e.preventDefault();
        });

        // Attach submit handler to dropdowns and hide 'apply' (redundant).
        $calendar.find('.form-submit').hide();

        $calendar.find('.form-item-demonz-calendar-month .form-select').change(function(e) {
          // Need to specify base for parseInt else Firefox (rightly) assumes
          // it's a radix number.
          // @see http://stackoverflow.com/questions/850341/how-do-i-work-around-javascripts-parseint-octal-behavior.
          month = parseInt($(this).val(), 10);
          Drupal.demonzCalendar.replaceCalendar(calendarID, year, month, $calendar);
          e.preventDefault();
        });

        $calendar.find('.form-item-demonz-calendar-year .form-select').change(function(e) {
          year = parseInt($(this).val(), 10);
          Drupal.demonzCalendar.replaceCalendar(calendarID, year, month, $calendar);
          e.preventDefault();
        });

      });
    }
  };

  Drupal.demonzCalendar = Drupal.demonzCalendar || {};

  Drupal.demonzCalendar.apiUrl = null;
  Drupal.demonzCalendar.loading = false;
  Drupal.demonzCalendar.loadedCalendar = [];

  Drupal.demonzCalendar.replaceCalendar = function (id, year, month, $calendar) {
    if (Drupal.demonzCalendar.loading) {
      alert("In the middle of a load, please wait until it's complete");
      return;
    }

    Drupal.demonzCalendar.loading = true;

    // Don't forget the leading zero!
    if (month < 10) {
      month = '0' + month;
    }

    var date = (year == 'now') ? 'now' : year + '-' + month;
    var calendarUrl = Drupal.demonzCalendar.apiUrl + id + '/' + date;

    // Attach loader.
    $loader = $('<div>').addClass('loader').html(Drupal.t('Loading...'));
    $calendar.prepend($loader);
    $calendar.addClass('loading');

    $.get(calendarUrl, function (data) {
      $calendar.replaceWith(data);
      Drupal.attachBehaviors($('.demonz-calendar').parent());
      Drupal.demonzCalendar.loading = false;
      $calendar.removeClass('loading');
      $loader.remove();
    });
  }

})(jQuery);